import React, { useState } from "react";

const App = () => {
  const [count, setCount] = useState(0);
  const onButtonClick = () => {
    setCount(count + 1);
  };

  return (
    <>
      <h1>Hello from React</h1>
      <p>Count: {count}</p>
      <button onClick={onButtonClick}>Click me</button>
    </>
  );
};

export default App;
